package Model;
public class Calculadora 
{
    private int capital;
    private int tasaInteres;
    private int agnos;

    public Calculadora(int capital, int tasaInteres, int agnos)
    {
        this.capital = capital;
        this.tasaInteres = tasaInteres;
        this.agnos = agnos;
    }

    public double getCapital() 
    {
        double v = this.capital;
        return v;
    }

    public double getTasaInteres() 
    {
        double v = (this.tasaInteres / 100d);
        return v;
    }

    public double getAgnos() 
    {
        double v = this.agnos;
        return v;
    }
    
    public double getInteres()
    {   
        double i = (getTasaInteres() / 100);
        double interes = (getCapital() * getTasaInteres()) * getAgnos();
        
        return interes;
    }
    
}
