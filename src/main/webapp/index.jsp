<%-- 
    Document   : index
    Created on : 24-04-2021, 19:44:26
    Author     : Wilson Fuentes W.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de interés simple</title>
        
        <link rel="stylesheet" href="css/style.css">
        <script src="js/funciones.js"></script>        
    </head>
<body>
    <br>
    <br>
    <br>
    <section class="container">
        <div class="box">

            <form name="frmCalculadora" action="ControllerCalculadora" method="POST">
                <table id="Tablaprincipal">
                    <tr>
                        <td colspan="2" class="CeldaTitulo">
                            <label class="Titulo">Calculadora de interés simple</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Capital</label>
                        </td>
                        <td>
                            <input type="text" name="txtCapital" id="txtCapital" class="TextBox" onkeypress="return SoloNumeros(event);" maxlength="9" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Tasa interés anual</label>
                        </td>
                        <td>
                            <input type="text" name="txtTasaInteres" id="txtTasaInteres" class="TextBox" onkeypress="return SoloNumeros(event);" maxlength="3" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Nº de Años</label>
                        </td>
                        <td>
                            <input type="text" name="txtAgnos" id="txtAgnos" class="TextBox" onkeypress="return SoloNumeros(event);" maxlength="2" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" name="btnCalcular" id="btnCalcular" class="Button">calcular</button>
                        </td>
                    </tr>                                
                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                    <tr>
                        <td colspan="2" name="tdMensaje" id="tdMensaje">

                        </td>
                    </tr>                                
                </table>
            </form>
        </div>
    </section>
</body>
</html>
