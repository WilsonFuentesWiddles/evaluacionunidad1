<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String error = (String)request.getAttribute("error");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="css/style.css">
        <script src="js/funciones.js"></script>
    </head>
    <body>
        <br>
        <br>
        <br>    
        <section class="container">
            <div class="box">
        
                <form name="frmError" action="ControllerResultado" method="POST">
                        <table id="Tablaprincipal">
                            <tr>
                                <td colspan="2" class="CeldaTitulo">
                                    <label class="Titulo">Error</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="CeldaLabel">
                                    <%= error %>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="SeparadorFila"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button type="submit" name="btnVolver" id="btnVolver" class="Button" >Volver</button>
                                </td>
                            </tr>                                
                        </table>
                </form>
            </div>
        </section>        
    </body>
</html>
