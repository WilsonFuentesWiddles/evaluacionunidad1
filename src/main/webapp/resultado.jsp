<%@page import="Model.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Calculadora cal = (Calculadora)request.getAttribute("calculadora");
    int capital = (int)cal.getCapital();
    int tasaInteres = (int)(cal.getTasaInteres()*100);
    int agnos = (int)cal.getAgnos();
    int intereses = (int)cal.getInteres();
    
    //String c = String.format("###.###", capital);
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de interés simple</title>
        
        <link rel="stylesheet" href="css/style.css">
        <script src="js/funciones.js"></script>   
    </head>
<body>
    <br>
    <br>
    <br>
    <section class="container">
        <div class="box">

            <form name="frmResultado" action="ControllerResultado" method="POST">
                <table id="Tablaprincipal">
                    <tr>
                        <td colspan="2" class="CeldaTitulo">
                            <label class="Titulo">Resultado Calculo de interés</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Capital</label>
                        </td>
                        <td>
                            <input type="text" name="txtCapital" id="txtCapital" class="TextBox" value="<%= capital %>" Readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Tasa interés anual</label>
                        </td>
                        <td>
                            <input type="text" name="txtTasaInteres" id="txtTasaInteres" class="TextBox" value="<%= tasaInteres %>" Readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Nº de Años</label>
                        </td>
                        <td>
                            <input type="text" name="txtAgnos" id="txtAgnos" class="TextBox" value="<%= agnos %>" Readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="CeldaLabel">
                            <label class="Label">Interés anual</label>
                        </td>
                        <td>
                            <input type="text" name="txtIntereses" id="txtIntereses" class="TextBox" value="<%= intereses %>" Readonly>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" name="btnVolver" id="btnVolver" class="Button" >Volver</button>
                        </td>
                    </tr>                                
                    <tr>
                        <td colspan="2" class="SeparadorFila"></td>
                    </tr>
                             
                </table>
            </form>
        </div>
    </section>
</body>
</html>
